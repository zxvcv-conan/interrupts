Changelog
=========

0.1.2 (2022-11-02)
------------------
- Documentation generated.
- Rewrite file interrupts.h.

0.1.1 (2022-10-12)
------------------
- Define names update.

0.1.0 (2022-09-27)
------------------
- Build system restructurization.
- Conan build settings updates.

0.0.1 (2022-07-09)
------------------
- Initial commit.
- Basic implementation.
