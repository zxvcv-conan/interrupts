


# API

[comment]: # ([[_DOCMD_USER_BLOCK_0_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_0_END]])

</br>





</br>

## Functions
***

[comment]: # ([[_DOCMD_USER_BLOCK_5_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_5_END]])

> ## irq_enable
> ***
> Enables interrupts.
>
> Weak declaration of function to enable interrupts.<br>If _ZXVCV_USE_INTERRUPTS is defined call for this function will enableprogram to been interrupted by interrupts.<br>Definition for this function should be done by the end user in his code.
>

</br>

> ## irq_disable
> ***
> Disables interrupts.
>
> Weak declaration of function to disable interrupts.<br>If _ZXVCV_USE_INTERRUPTS is defined call for this function will disableprogram to been interrupted by interrupts.<br>Definition for this function should be done by the end user in his code.
>

</br>



[comment]: # ([[_DOCMD_USER_BLOCK_6_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_6_END]])

***
[Main page](../README.md) | [Repository url](https://gitlab.com/zxvcv-conan/interrupts) </br>
***
<i>Generated with <b>docmd</b> Python package.</i>
