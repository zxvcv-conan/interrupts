// ====================================================================================
//                                LICENSE INFORMATIONS
// ====================================================================================
// Copyright (c) 2020-2022 Pawel Piskorz
// Licensed under the Eclipse Public License 2.0
// See attached LICENSE file
// ====================================================================================


#ifndef ZXVCV_INTERRUPTS_H_
#define ZXVCV_INTERRUPTS_H_


// =================================== INCLUDES =======================================


// ================================== DATA TYPES ======================================


// ============================== PUBLIC DECLARATIONS =================================
#ifdef _ZXVCV_USE_INTERRUPTS
    /** Enables interrupts.
     *
     * Weak declaration of function to enable interrupts.<br>
     * If _ZXVCV_USE_INTERRUPTS is defined call for this function will enable
     * program to been interrupted by interrupts.<br>
     * Definition for this function should be done by the end user in his code.
     */
    void __attribute__((weak)) irq_enable(void);

    /** Disables interrupts.
     *
     * Weak declaration of function to disable interrupts.<br>
     * If _ZXVCV_USE_INTERRUPTS is defined call for this function will disable
     * program to been interrupted by interrupts.<br>
     * Definition for this function should be done by the end user in his code.
     */
    void __attribute__((weak)) irq_disable(void);
#endif // _ZXVCV_USE_INTERRUPTS

#endif // ZXVCV_INTERRUPTS_H_
