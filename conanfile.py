from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout


class ConanPackage(ConanFile):
    name = "interrupts"
    version = "0.1.2"

    # Optional metadata
    license = "Eclipse Public License - v 2.0"
    author = "Pawel Piskorz ppiskorz0@gmail.com"
    url = "https://gitlab.com/zxvcv-conan/interrupts"
    description = """Interrupts codes.
    Templates implementation for interrupts.
    """
    topics = ("interrupts", "C", "embedded")

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "include/*"
    no_copy_source = True

    def package(self):
        self.copy("*.h")
